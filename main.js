process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/**
 * Principal function
 * @returns {undefined}
 */
function main() {
    console.time('Test performance');
    var numberTestCases = readLine();
    var testCases = getTestCases(numberTestCases);
    testCases.forEach(testCaseIslands=>getTotalArchipielago(testCaseIslands));
    console.timeEnd('Test performance');
}
/**
 * Getting the archipielagos based on islands
 * @param {type} islands
 * @returns {Array}
 */
function getTotalArchipielago(islands) {
    var archipielagos = [];
    for (var i = 0; i < islands.length; i++) {
        var endpointIsland = islands[i];
        var neighborDistances = [];
        for (var j = 0; j < islands.length; j++) {
            if (j === i) {
              continue;
            }
            var neighbor = islands[j];
            var distanceEndPoint = islandDistance(
                    endpointIsland.x, endpointIsland.y,
                    neighbor.x, neighbor.y);
            neighborDistances.push({neighbor, distanceEndPoint});
        }
        evaluateSegments(neighborDistances, endpointIsland, archipielagos);
    }
    console.log(archipielagos.length);
}

/**
 * Function to evaluate the segments of each island(endPoint
 * @param {array} neighborDistances total of distances from the endPointIsland
 * @param {type} endpointIsland endpoint to get his distances from other islands
 * @param {type} archipielagos array of archipielagos to obtain
 * @returns {void}
 */
function evaluateSegments(neighborDistances, endpointIsland, archipielagos) {
  for (var j = 0; j < neighborDistances.length; j++) {
    var neighbor1 = neighborDistances[j];
    for (var k = j; k < neighborDistances.length; k++) {
      if (k === j ) {
        continue;
      }
      var neighbor2 = neighborDistances[k];
      if (neighbor1.distanceEndPoint === neighbor2.distanceEndPoint) {
        var archipielago = [
          endpointIsland,
          neighbor1.neighbor,
          neighbor2.neighbor
        ];
        if (!existArchipielago(archipielagos, archipielago)) {
          archipielagos.push(archipielago);
        }
      }
    }
  }
}

/**
 * Evaluate if exist archipielagos
 * @param {type} archipielagos
 * @param {type} archipielago
 * @returns {undefined}
 */
function existArchipielago(archipielagos, archipielago) {
  var exist = false;
  for (var i = 0; i < archipielagos.length; i++) {
    var archipielagoA = archipielagos[i];
    if (IsArchipielagoEquivalent(archipielagoA, archipielago)) {
      exist = true;
      break;
    }
  }
  return  exist;
}

/**
 * Object to compare the archipielagos in base of each island contained
 * @param {type} archipielagoA
 * @param {type} archipielagoB
 * @returns {Boolean}
 */
function IsArchipielagoEquivalent(archipielagoA, archipielagoB) {
  var isEquivalent = true;
  for (var i = 0; i < archipielagoB.length; i++) {
    if (archipielagoA.indexOf(archipielagoB[i]) === -1) {
      isEquivalent = false;
      break;
    }
  }
  return isEquivalent;
}
/**
 * Calculating the distance between islands
 * @param {type} x1
 * @param {type} y1
 * @param {type} x2
 * @param {type} y2
 * @returns {Number}
 */
function islandDistance(x1, y1, x2, y2) {
    if (!x2)
        x2 = 0;
    if (!y2)
        y2 = 0;
    return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

/**
 * Separating the test cases
 * @param {type} numberTestCases
 * @returns {Array}
 */
 
function getTestCases(numberTestCases) {
    var testCases = [numberTestCases];
    for (var testCaseIterator = 0; testCaseIterator < numberTestCases; testCaseIterator++) {
        var islandsTestCase = [];
        var numberOfIslands = readLine();
        for (var i = 0; i < numberOfIslands; i++) {
            var islandPositions = readLine();
            islandsTestCase.push({
                x: parseInt(islandPositions.split(' ')[0]),
                y: parseInt(islandPositions.split(' ')[1])
            });
        }
        testCases[testCaseIterator]=islandsTestCase;
    }
    return testCases;
}


